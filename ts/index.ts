import * as plugins from './smartmatch.plugins.js';

export class SmartMatch {
  public wildcard: string;
  constructor(wildcardArg: string) {
    this.wildcard = wildcardArg;
  }

  public match(matchStringArg: string): boolean {
    return plugins.matcher.isMatch(matchStringArg, this.wildcard);
  }
}
