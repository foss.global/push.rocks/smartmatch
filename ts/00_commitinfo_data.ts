/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartmatch',
  version: '2.0.0',
  description: 'a minimal matching library using picomatch'
}
