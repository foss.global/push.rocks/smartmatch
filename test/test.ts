import { expect, tap } from '@pushrocks/tapbundle';
import * as smartmatch from '../ts/index.js';

tap.test('first test', async () => {
  const smartMatchInstance = new smartmatch.SmartMatch('*.any');
  const isAMatch = smartMatchInstance.match('wow.any');
  const isNotAMatch = smartMatchInstance.match('wow.not');

  expect(isAMatch).toBeTrue();
  expect(isNotAMatch).toBeFalse();
});

tap.start();
